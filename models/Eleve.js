class Eleve {
    constructor(nom,prenom,age,fkecole){
        this.id;
        this.nom= nom;
        this.prenom = prenom;
        this.age = age ;
        this.fkecole = fkecole ; 
    }
    fromJson(json){
        this.id = json["id"];
        this.nom = json["nom"];
        this.prenom = json["prenom"];
        this.age = json["age"];
        this.fkecole = json["fkecole"];
    }
}
module.exports = Eleve;