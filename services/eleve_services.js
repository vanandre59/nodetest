const EleveModel = require('../models/Eleve');
const db = require('../config/db');

async function getAllEleveWithAsync() {
   
    try {
        const eleves = [];
        const result = await db.awaitQuery('SELECT * FROM eleve');
        for (let x = 0; x < result.lenght; x++) {
            let eleve = new EleveModel();
            eleve.fromJson(result[x]);
            eleves.push(eleve);
        }
        console.log("HERE");
        return {"status":200,"data":eleves};
    } catch (e) {
        return {
            "status": 400,
            "error": e.toString()
        };
    }

}

async function getEleveById(id) {
    
    try {
        const result = await db.awaitQuery('SELECT * FROM eleve WHERE id = ?', [id]);
            let eleve = new EleveModel();
            eleve.fromJson(result);
        return {"status":200,"data":eleve};
    } catch (e) {
        return {
            "status": 400,
            "error": e.toString()
        };
    }
}
async function updateEleve(eleve) {
    const result = await db.awaitQuery("Update eleve set nom = ? , prenom = ? , age = ? where id = ? " , [eleve.nom , eleve.prenom, eleve.age,eleve.id]);
}
async function addEleve(eleve) {
   const result = await db.awaitQuery(`INSERT INTO eleve (nom, prenom , age) VALUES (? , ?, ?)`,[eleve.nom, eleve.prenom , eleve.age]);
}
async function deleteEleve(id){
    
    const result = await db.awaitQuery("DELETE FROM eleve WHERE id = ?", [id]);
}
module.exports = {
    
    getEleveById,
    updateEleve,
    addEleve,
    getAllEleveWithAsync,
    deleteEleve
}