const mysql = require('mysql-await');
require('dotenv').config();
console.log(process.env.USER_SQL);
const db = mysql.createConnection({
    "host": process.env.ADDRESS,
    "user": process.env.USER_SQL,
    "password": process.env.PASSWORD,
    "database": process.env.DATABASE,
});
db.connect(function (err) {
    if (err) throw err;
    console.log('connect');
});
module.exports=db;