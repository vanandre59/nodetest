const express = require('express');
const app = express();
const port = 3000;
const bodyparser = require('body-parser');
const ecoleRouter = require('./route/ecole_route');
const eleveRouter = require('./route/eleve_route')
const mysqlAwait = require('mysql-await');

app.use(bodyparser.json());
app.use("/eleve",eleveRouter);
app.use("/ecole",ecoleRouter);


app.get('/', (req, res) => {
    res.send('Hello World!')
  });

app.listen(port,()=> {
    console.log('Voici mon server')
});