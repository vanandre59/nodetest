const express = require('express');
const router = express.Router();
const eleveServices = require("../services/eleve_services");
const EleveModel = require("../models/eleve");


router.get('/all', async (req, res) => {
    const result = await eleveServices.getAllEleveWithAsync();
    res.json(result);
    // le res doit renvois la donner dans le service =<utiliser res.json(.result)
  });
  
router.get('/:id', async (req, res) => {
    const result = await eleveServices.getEleveById(req.params["id"]);
    res.json(result);
  });
router.post('/add', (req, res) => { 
    let eleve = new EleveModel()
    console.log(req.body);
    eleve.fromJson(req.body);
    eleveServices.addEleve(eleve);
    res.json('ajout l eleve ok')});
  
router.put('/update', async (req, res) => {
    let eleve = new EleveModel()
    eleve.fromJson(req.body);
    console.log(eleve);
    await eleveServices.updateEleve(eleve);
    res.send('vos données ont bien ete mise a jour')
});


router.delete("/delete/:id", async (req, res) => {
  try {
    const id = req.params.id;
    await eleveServices.deleteEleve(id);
    res.send('supp de l eleve ok')
  } catch (e) { console.log(e);
    res.send('loupé')}
});

module.exports = router ;