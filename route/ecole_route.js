const express = require('express');
const router = express.Router();
const ecoleServices = require("../services/ecole_services");
const EcoleModel = require("../models/Ecole");


router.get('/all', async (req, res) => {
  const result = await ecoleServices.getAllEcoleWithAsync();
  res.json(result);
  // le res doit renvois la donner dans le service =<utiliser res.json(.result)
});
  
router.get('/all/:id', (req, res) => {
    console.log(req.params);
    res.send('Im a student n° '+req.params["id"])
  });
router.post('/add', (req, res) => {
    
    let ecole = new EcoleModel()
    console.log(req.body);
    ecole.fromJson(req.body);
    ecoleServices.addEcole(ecole);
    res.json('ajout d\'ecole ok')});

router.delete("/delete/:id", async (req, res) => {
  try {
    const id = req.params.id;
    await ecoleServices.deleteEcole(id);
    res.send('supp de l ecole ok')
  } catch (e) { console.log(e);
    res.send('loupé')}

});

module.exports = router ;